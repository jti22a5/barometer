﻿using Barometer.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Barometer.Model;
using Bing.Maps;
using Windows.UI.Xaml.Controls;
using Windows.Devices.Geolocation;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.Storage;

namespace Barometer.Controller
{
    /// <summary>
    /// De Controller die de GUI delen regelt
    /// </summary>
    class GUIController
    {
        private Main GUI;
        private GPSInput GpsInput;
        private Frame rootFrame;
        public string[] languages;
        public string[] languageFlagImages;
        private int languageIndex = 1;
        public static bool routeFlag = true;
        public bool OutOfBoundsFlag { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public GUIController()
        {
            this.GpsInput = new GPSInput();
            GpsInput.Initialize();
            this.languages = new string[] { "nl-NL", "en-US", "ja-JP" };
            this.languageFlagImages = new string[] { "NLVlag.png", "ENVlag.png", "JPVlag.png" };
            this.OutOfBoundsFlag = false;
        }

        /// <summary>
        /// Sets root frame to given one
        /// </summary>
        /// <param name="rootFrame"></param>
        public void setRootFrame(Frame rootFrame)
        {
            this.rootFrame = rootFrame;
        }

        /// <summary>
        /// Sets main page to given one
        /// </summary>
        /// <param name="main"></param>
        public void setMainPage(Main main)
        {
            GUI = main;    

            // Initialize user input
            Barometer.getInputHandler().Initialize();

            /*showSightInfo(new Sight()
            {
                Name = "Maarten",
                State = Sight.EnumState.Visited,
                Description = "Maarten is een enorme bezienswaardigheid. Met een IQ lager dan 5 en een hoofd in de vorm van een ei is dit een must see voor het hele gezin.",
                OpeningDescription = "24/7",
                Location = new LocationDb(){Latitude = 51.501584, Longitude = 3.912821},
                BuildYear = new DateTime(1995, 06, 11),
                Media = new Dictionary<Sight.MediaEnum, Media>()
                {
                    {
                        Sight.MediaEnum.MEDIA_IMAGE, new Model.Image() 
                        {
                            SourceImage = new Windows.UI.Xaml.Media.Imaging.BitmapImage() 
                            {
                                UriSource = new Uri("ms-appx:///Assets/NLVlag.png")
                            }
                        }
                    }
                }
            });*/
        }

        /// <summary>
        /// Navigates to given page
        /// </summary>
        public void navigateToPage(Type type)
        {
            //if (rootFrame == null)
            //{
            //    return;
            //}
         
            rootFrame.Navigate(type);
        }

        public async void askForSession()
        {
            StorageFile pStorageFile = null;
            
            // Ask to load a session
            try
            {
                pStorageFile = await ApplicationData.Current.LocalFolder.GetFileAsync("session.dat");
            }
            catch
            {
                Controller.Barometer.getGUIController().navigateToPage(typeof(View.SelectRoute));
                return;
            }

            MessageDialog pMessageDialog = new MessageDialog("Wilt u een eerdere sessie laden?", "Vraag");
            pMessageDialog.Commands.Add(new UICommand("Ja", new UICommandInvokedHandler(delegate
            {
                Controller.Barometer.loadDataFromSavedSession();
            })));
            pMessageDialog.Commands.Add(new UICommand("Nee", new UICommandInvokedHandler(delegate 
            { 
                Controller.Barometer.getGUIController().navigateToPage(typeof(View.SelectRoute));
 
                // Delete session file
                pStorageFile.DeleteAsync();
            })));

            try
            {
                await pMessageDialog.ShowAsync();
            }
            catch
            {

            }
        }

        [Obsolete]
        /// <summary>
        /// Zet data uit de database in specifieke velden van de GUI. Deze methode wordt aangeroepen nadat de database is aangemaakt.
        /// </summary>
        public void setDatabaseData()
        {
            if (Controller.Barometer.getSession().Route != null)
            {
                GUI.setRouteName(Controller.Barometer.getSession().Route.Name);
            }
        }

        /// <summary>
        /// Toggles settings button on main page
        /// </summary>
        public void toggleSettingsButton()
        {
            Windows.UI.Xaml.Controls.Button pButton = getMainPage().SettingsButton;

            if (pButton.Visibility == Visibility.Collapsed)
            {
                pButton.Visibility = Visibility.Visible;
            }
            else
            {
                pButton.Visibility = Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Shows settings menu
        /// </summary>
        public void showSettingsMenu()
        {
            new View.Settings().ShowIndependent();
        }

        /// <summary>
        /// Returns mainpage
        /// </summary>
        /// <returns></returns>
        public Main getMainPage()
        {
            return GUI;
        }

        public async void GUI_GPSLocationRequest()
        {
            // TO DO: remove this and make it actual GPS data
            //GUI.DrawCurrentLocation(new Location(51.490255, 3.910915));
            //return;

            try
            {
                await GpsInput.ProcessCurrentLocation();
                GUI.DrawCurrentLocation(GpsInput.GetCurrentLocation());
            }
            catch(Exception)
            {
                Barometer.ShowError(Controller.Barometer.getSession().RMap.GetValue("gpsErrorTitle", Controller.Barometer.getSession().Context).ValueAsString, Controller.Barometer.getSession().RMap.GetValue("gpsErrorDescription", Controller.Barometer.getSession().Context).ValueAsString);
            }
        }

        /// <summary>
        /// Geeft de event argumenten door van het PositionChanged event uit GPSInput > Geolocator. De GUI (Main) moet deze vervolgens gaan tekenen.
        /// </summary>
        /// <param name="geolocator"></param>
        /// <param name="args"></param>
        public void drawLocationPosition(Geolocator geolocator, PositionChangedEventArgs args)
        {
            GUI.geolocatorPositionChanged(geolocator, args);
        }

        /// <summary>
        /// Shows info of the given sight
        /// </summary>
        /// <param name="pSight"></param>
        public void showSightInfo(Sight pSight)
        {
            GUI.SightFrame.Content = new View.SightInfo(pSight);
        }

        /// <summary>
        /// 
        /// </summary>
        public void showTutorial()
        {
            GUI.TutorialFrame.Content = new View.Tutorial();
            GUI.TutorialFrame.Width = Window.Current.Bounds.Width;
            GUI.TutorialFrame.Height = Window.Current.Bounds.Height;
        }

        /// <summary>
        /// Hides sight info
        /// </summary>
        public void hideSightInfo()
        {
            GUI.SightFrame.Content = null;
        }

        public void hideTutorial()
        {
            GUI.TutorialFrame.Content = null;
        }

        public void setLanguage()
        {            
            Controller.Barometer.getSession().Context.Languages = new string[] { Controller.Barometer.getGUIController().languages[(this.languageIndex)] };
            GUI.ToggleLanguage(this.languageIndex);
            
            this.languageIndex = (this.languageIndex + 1) % Controller.Barometer.getGUIController().languages.Length;
            Controller.Barometer.getSession().LanguageId = this.languageIndex;
        }

        public void setStopButton(bool setting)
        {
            GUI.ToggleStopButton(setting);
        }
    }
}
