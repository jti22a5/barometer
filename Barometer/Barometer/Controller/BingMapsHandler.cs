﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bing.Maps;
using Windows.UI.Xaml.Controls;
using Barometer.Model;
using Bing.Maps.Directions;
using Windows.Devices.Geolocation;
using Windows.Devices.Geolocation.Geofencing;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI;


namespace Barometer.Controller
{
    public class BingMapsHandler
    {
        private Map map;        
        private DirectionsManager DirManager;
        private Model.Route route;
        private InputHandler inputHandler;

        public BingMapsHandler()
        {

        }

        public void Initialize()
        {
            if (map.Children.Count > 0)
            {
                Clear();
            }                        
            GeofenceMonitor.Current.Geofences.Clear();
            GeofenceMonitor.Current.GeofenceStateChanged += Current_Geofence_Entered;

            if (Barometer.getSession().Route != null && Barometer.getSession().Route.Sights != null)
            {
                this.route = Barometer.getSession().Route;
            }
    
            DirManager = map.DirectionsManager;
            if (route != null)
            {
                foreach (Sight s in route.Sights)
                {
                    AddWaypoints(s);
                    AddPushPin(s);

                    if (s.Name != "Aanwijzing") 
                    {
                        //AddGeofenceToSight(s);
                    }
                }
                DrawRoute();
            }
        }
        
        public void SetMap(Map map)
        {
            this.map = map;

            map.Credentials = "Au4T_jPtx9b50iGsXJwEUMeO3iJUQgDwk1rOWcaYeGIkEp4jYCjJWyQf7TY-rbXO";
        }

        /// <summary>
        /// Ruimt de lijsten van de singleton object op
        /// </summary>
        public void Clear()
        {
            map.Children.Clear();

            if (DirManager.ActiveRoute != null)
            {
                DirManager.HideRoutePath(DirManager.ActiveRoute);
            }
        }

        /// <summary>
        /// Voegt de waypoints toe aan de directionManager.
        /// Maakt pushpins aan.
        /// </summary>
        private void AddWaypoints(Sight s)
        {
            if (s.Location != null)
            {
                Waypoint point = new Waypoint(s.Location.getBingMapLocation());
                DirManager.Waypoints.Add(point);
            }
        }

        /// <summary>
        /// Tekend de route tussen de aangemaakte waypoints.
        /// </summary>
        private async void DrawRoute()
        {
            if(DirManager.Waypoints.Count > 0)
            {
                for (int i = 0; i < DirManager.Waypoints.Count; i++ )
                {
                    Waypoint wp = DirManager.Waypoints[i];
                    if (i == (DirManager.Waypoints.Count - 1) || i == 0)
                    {
                        wp.IsViaPoint = false;
                    }
                    else { 
                        wp.IsViaPoint = !((i % 10) == 0);
                    }                    
                }

                DirManager.RenderOptions.WaypointPushpinOptions.Visible = false;
                DirManager.RequestOptions.RouteMode = RouteModeOption.Walking;
                DirManager.RequestOptions.Optimize = OptimizeOption.Walking;

                RouteResponse Route_response = await DirManager.CalculateDirectionsAsync();
                if(Route_response.HasError){
                    Barometer.ShowError(Controller.Barometer.getSession().RMap.GetValue("RouteErrorTitle", Controller.Barometer.getSession().Context).ValueAsString, Controller.Barometer.getSession().RMap.GetValue("RouteErrorDescription", Controller.Barometer.getSession().Context).ValueAsString);
                }
                else
                {
                    DirManager.ShowRoutePath(DirManager.ActiveRoute);
                }
                
            }
            
        }

        /// <summary>
        ///     Deze functie voegt op de Bingmaps een pinpoint toe. De Name property van een pushpin moet uniek zijn.
        /// </summary>
        /// <param name="loc">Hier wordt de Lokatie aangegeven</param>
        /// <param name="name">Hier wordt de naam van de pinpoint aangegeven</param>
        /// <param name="img">Hier staat het plaatje die moet op het map wordt aangegeven pinpoint.</param>
        private void AddPushPin(Sight s)
        {
            if(s != null && s.Location != null){
                Pushpin pin = new Pushpin()
                {
                    Name = s.Sequence.ToString(),
                    Text = s.Name,
                    Tag = s
                };

                if(s.Name.Equals("Aanwijzing")){
                    pin.Background = new SolidColorBrush(Color.FromArgb(100,100,100, 1));
                }

                pin.Tapped += new TappedEventHandler(pin_Tapped);
                MapLayer.SetPosition(pin, s.Location.getBingMapLocation());
                map.Children.Add(pin);

                if (s.State == Sight.EnumState.Viewed)
                {
                    // Obvious re-load from session
                    ChangePushPinState(s);
                }
            }
        }

        /// <summary>
        /// Event voor de Pushpin die aangeklikt wordt, Dit event laat informatie zien over de Sight.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="arg"></param>
        private void pin_Tapped(object sender, TappedRoutedEventArgs arg)
        {
            Pushpin pin;
            Control c = sender as Control;
            if (c.GetType() == typeof(Pushpin))
            {
                pin = c as Pushpin;
                Barometer.getGUIController().showSightInfo(pin.Tag as Sight);
                
                // Uncomment for testing ^_^
                //ChangePushPinState(pin.Tag as Sight);
            }
        }

        /// <summary>
        /// voegt geofences toe en een event voor de GeofenceMonitor. Net zoals de pushpin, moet de name property van een geofence uniek zijn.
        /// </summary>
        /// <param name="s"></param>
        private void AddGeofenceToSight(Sight s)
        {
            if(s != null && s.Location != null){
                BasicGeoposition pos = new BasicGeoposition()
                {
                    Latitude = s.Location.Latitude,
                    Longitude = s.Location.Longitude
                };
                Geocircle circle = new Geocircle(pos, 12.0);
                
                TimeSpan notificationTrigger = TimeSpan.FromSeconds(3);
                Geofence fence = new Geofence(s.Sequence.ToString() + "_" + DateTime.Now.Ticks + "_Geofence", circle, MonitoredGeofenceStates.Entered, true, notificationTrigger);
                GeofenceMonitor.Current.Geofences.Add(fence);                
            }
        }

        bool m_bActive = false;

        [Obsolete]
        /// <summary>
        /// GeofenceMonitor event wat er gebeurt als dit event plaatsvind;
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="arg"></param>
        private async void Current_Geofence_Entered(GeofenceMonitor sender, object arg)
        {
            var reports = sender.ReadReports();

            await Controller.Barometer.getGUIController().getMainPage().Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
             {
                 foreach(var report in reports)
                 {
                     var state = report.NewState;
                     var geofence = report.Geofence;

                     System.Diagnostics.Debug.WriteLine("Hit (" + state + ")");

                     if (state == GeofenceState.Entered)
                     {
                         System.Diagnostics.Debug.WriteLine(geofence.Id + " has been hit");

                         int geofenceID = int.Parse(geofence.Id.Split('_')[0]);

                         if (Controller.Barometer.getSession().Route != null)
                         {
                             if (Controller.Barometer.getSession().Route.Sights != null)
                             {
                                 foreach (Sight sight in Controller.Barometer.getSession().Route.Sights)
                                 {
                                     if (sight.Sequence == geofenceID)
                                     {
                                         System.Diagnostics.Debug.WriteLine(sight.Name + " has been struck");
                                         Controller.Barometer.getGUIController().showSightInfo(sight);                                     
                                         ChangePushPinState(sight);
                                         return;
                                     }
                                 }

                                 if (!m_bActive)
                                 {
                                     Controller.Barometer.ShowError("I have been triggered with id " + geofenceID, "Trekker");
                                     m_bActive = true;
                                 }
                             } 
                         }
                     }                     
                 }
             });            
        } 

        /// <summary>
        /// Deze veranderd de kleur van de pushpin zodra deze bezocht is.
        /// </summary>
        /// <param name="sight">Geef hier de bezienswaardigheid aan en die veranderd dan naar de state die nodig is</param>
        public void ChangePushPinState(Sight sight)
        {

            //Change the sight status
            if (Controller.Barometer.getSession().Route != null)
            {
                if (Controller.Barometer.getSession().Route.Sights != null)
                {
                    foreach (Model.Sight s in Controller.Barometer.getSession().Route.Sights)
                    {
                        if(s.Sequence.Equals(sight.Sequence))
                        {
                            s.State = Sight.EnumState.Viewed;
                        }
                    }
                }
            }

            // Toggle color
            foreach (object pBing in map.Children)
            {
                if (pBing is Pushpin)
                {
                    Pushpin pPushpin = (Pushpin)pBing;

                    if (pPushpin.Name == sight.Sequence.ToString())
                    {
                        pPushpin.Background = new SolidColorBrush(Color.FromArgb(255, 0x66, 0x66, 0x66));
                        break;
                    }
                }                
            }
        }

        [Obsolete]
        /// <summary>
        /// Geeft weer hoeveel afstand er nog gelopen moet worden
        /// </summary>
        /// <returns>De aantal kilometer in een decimaal getal</returns>
        public double GetRemainingDistance()
        {
            return 0.00;
        }

        /// <summary>
        /// Geeft het aantal kilometers terug die de route zal zijn.
        /// </summary>
        /// <param name="route">Geeft hier een route mee die hij moet uitrekenen hoeveel kilomter dit zal zijn</param>
        /// <returns>De aantal kilometer in een decimaal getal</returns>
        public double GetTotalDistance()
        {
            if(this.map.DirectionsManager.ActiveRoute != null)
            {
                return this.map.DirectionsManager.ActiveRoute.TravelDistance;
            }
            return 0.00;
        }

        /// <summary>
        /// Deze functie geeft hoeveel tijd er is totdat de gebruiker op de eindlocatie aankomt. Een persoon loopt gemiddeld 4 kilometer per uur.
        /// </summary>
        /// <param name="route"></param>
        /// <returns>Het aantal minuten</returns>
        public double GetRemainingTime()
        {
            return (GetTotalDistance() / 4) * 60;
        }

        /// <summary>
        /// Geeft de map terug die wordt gebruikt.
        /// </summary>
        /// <returns>geeft de map terug.</returns>
        public Map GetMap()
        {
            return this.map;
        }
    }
}
