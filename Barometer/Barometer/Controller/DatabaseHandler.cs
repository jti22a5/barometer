﻿using Barometer.Model;
using Bing.Maps;
using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Streams;

namespace Barometer.Controller
{
    public class DatabaseHandler
    {
        public List<Route> CacheRoutes;
        public Session CacheSession;
        public bool Exists;

        public DatabaseHandler()
        {
            CacheRoutes = new List<Route>();
            Route r = new Route() { 
                Sights = this.generateSightShort(),
                Name = "Historische Route (hc)",
                ID = 1
            };
            CacheRoutes.Add(r);
        }

        public List<Route> GetRoutes()
        {
            return CacheRoutes;
        }

        public async Task<bool> SaveSession(Session s)
        {
            try
            {
                using (MemoryStream sessionData = new MemoryStream())
                {
                    DataContractSerializer serializer = new DataContractSerializer(typeof(Session));
                    serializer.WriteObject(sessionData, s);

                    StorageFile file = await ApplicationData.Current.LocalFolder.CreateFileAsync("session.dat", CreationCollisionOption.ReplaceExisting);
                    using (Stream fileStream = await file.OpenStreamForWriteAsync())
                    {
                        sessionData.Seek(0, SeekOrigin.Begin);
                        await sessionData.CopyToAsync(fileStream);
                        await fileStream.FlushAsync();
                    }
                }
                return true;
            }
            catch { return false; }
            
        }

        public async Task<Session> LoadSession()
        {
            StorageFile file = await ApplicationData.Current.LocalFolder.
                           GetFileAsync("session.dat");
            using (IInputStream inStream = await file.OpenSequentialReadAsync())
            {
                DataContractSerializer serializer =
                        new DataContractSerializer(typeof(Session));
                return serializer.ReadObject(inStream.AsStreamForRead()) as Session;
            }
        }

        public bool createSightWithRoute(Sight s, Route r)
        {
            if(this.GetRoutes() != null){
                foreach(Route r1 in this.GetRoutes()){
                    if(r1.Equals(r)){
                        r1.Sights.Add(s);
                        return true;
                    }
                }
            }
            return false;
        }

        private List<Sight> generateSights()
        {
            List<Sight> sights = new List<Sight>();
            sights.Add(new Sight()
            {
                Sequence = 1,
                Location = new LocationDb() { Latitude = 51.59380, Longitude = 4.77963 },
                Name = "VVV Breda",
                Description = "Beginpunt",
            });
            sights.Add(new Sight()
            {
                Sequence = 2,
                Location = new LocationDb() { Latitude = 51.59307, Longitude = 4.77969 },
                Name = "Liefdeszuster",
                Description = "",
            });
            sights.Add(new Sight()
            {
                Sequence = 3,
                Location = new LocationDb() { Latitude = 51.59250, Longitude = 4.77969 },
                Name = "Aanwijzing",
                Description = "Valkenberg",
            });
            sights.Add(new Sight()
            {
                Sequence = 4,
                Location = new LocationDb() { Latitude = 51.59250, Longitude = 4.77969 },
                Name = "Nassau-Baroniemonument",
                Description = "",
            });
            sights.Add(new Sight()
            {
                Sequence = 5,
                Location = new LocationDb() { Latitude = 51.59256, Longitude = 4.77889 },
                Name = "The Light House",
                Description = "",
            });
            sights.Add(new Sight()
            {
                Sequence = 6,
                Location = new LocationDb() { Latitude = 51.59265, Longitude = 4.77844 },
                Name = "Aanwijzing",
                Description = "1e bocht Valkenberg",
            });
            sights.Add(new Sight()
            {
                Sequence = 7,
                Location = new LocationDb() { Latitude = 51.59258, Longitude = 4.77806 },
                Name = "Aanwijzing",
                Description = "2e bocht Valkenberg",

            });
            sights.Add(new Sight()
            {
                Sequence = 8,
                Location = new LocationDb() { Latitude = 51.59059, Longitude = 4.77707 },
                Name = "Aanwijzing",
                Description = "Einde park",
            });
            sights.Add(new Sight()
            {
                Sequence = 9,
                Location = new LocationDb() { Latitude = 51.59061, Longitude = 4.77624 },
                Name = "Kasteel van Breda",
                Description = "",
            });
            sights.Add(new Sight()
            {
                Sequence = 10,
                Location = new LocationDb() { Latitude = 51.58992, Longitude = 4.77634 },
                Name = "Stadhouderspoort",
                Description = "",
            });
            sights.Add(new Sight()
            {
                Sequence = 11,
                Location = new LocationDb() { Latitude = 51.59033, Longitude = 4.77623 },
                Name = "Aanwijzing",
                Description = "Kruising Kasteelplein/Cingelstraat",

            });
            sights.Add(new Sight()
            {
                Sequence = 12,
                Location = new LocationDb() { Latitude = 51.59043, Longitude = 4.77518 },
                Name = "Huis van Brecht (rechter zijde)",
                Description = "1e bocht Cingelstraat",
            });
            sights.Add(new Sight()
            {
                Sequence = 13,
                Location = new LocationDb() { Latitude = 51.59000, Longitude = 4.77429 },
                Name = "Aanwijzing",
                Description = "2e bocht Cingelstraat",

            });
            sights.Add(new Sight()
            {
                Sequence = 14,
                Location = new LocationDb() { Latitude = 51.59010, Longitude = 4.77336 },
                Name = "Spanjaardsgat (rechter zijde)",
                Description = "",
            });
            sights.Add(new Sight()
            {
                Sequence = 15,
                Location = new LocationDb() { Latitude = 51.58982, Longitude = 4.77321 },
                Name = "Begin Vismarkt",
                Description = "",
            });
            sights.Add(new Sight()
            {
                Sequence = 16,
                Location = new LocationDb() { Latitude = 51.58932, Longitude = 4.77444 },
                Name = "Begin Havermarkt",
                Description = "",
            });
            sights.Add(new Sight()
            {
                Sequence = 17,
                Location = new LocationDb() { Latitude = 51.58872, Longitude = 4.77501 },
                Name = "Aanwijzing",
                Description = "Driehoek Kerkplein 1",

            });
            sights.Add(new Sight()
            {
                Sequence = 18,
                Location = new LocationDb() { Latitude = 51.58878, Longitude = 4.77549 },
                Name = "Grote Kerk",
                Description = "Driehoek Kerkplein 2",
            });
            sights.Add(new Sight()
            {
                Sequence = 19,
                Location = new LocationDb() { Latitude = 51.58864, Longitude = 4.77501 },
                Name = "Aanwijzing",
                Description = "Driehoek Kerkplein 3",

            });
            sights.Add(new Sight()
            {
                Sequence = 20,
                Location = new LocationDb() { Latitude = 51.58822, Longitude = 4.77525 },
                Name = "Het poortje",
                Description = "",

            });
            sights.Add(new Sight()
            {
                Sequence = 21,
                Location = new LocationDb() { Latitude = 51.58716, Longitude = 4.77582 },
                Name = "Ridderstraat",
                Description = "",

            });
            sights.Add(new Sight()
            {
                Sequence = 22,
                Location = new LocationDb() { Latitude = 51.58747, Longitude = 4.77662 },
                Name = "Grote Markt",
                Description = "",
            });
            sights.Add(new Sight()
            {
                Sequence = 23,
                Location = new LocationDb() { Latitude = 51.58771, Longitude = 4.77652 },
                Name = "Het Wit Lam",
                Description = "",

            });
            sights.Add(new Sight()
            {
                Sequence = 24,
                Location = new LocationDb() { Latitude = 51.58797, Longitude = 4.77638 },
                Name = "Bevrijdingsmonument",
                Description = "",
            });
            sights.Add(new Sight()
            {
                Sequence = 25,
                Location = new LocationDb() { Latitude = 51.58885, Longitude = 4.77616 },
                Name = "Stadshuis",
                Description = "",

            });
            sights.Add(new Sight()
            {
                Sequence = 26,
                Location = new LocationDb() { Latitude = 51.58883, Longitude = 4.77617 },
                Name = "Aanwijzing",
                Description = "Kruising Grote Markt / Stadserf",

            });
            sights.Add(new Sight()
            {
                Sequence = 27,
                Location = new LocationDb() { Latitude = 51.58889, Longitude = 4.77659 },
                Name = "Aanwijzing",
                Description = "Achterkant stadshuis",

            });
            sights.Add(new Sight()
            {
                Sequence = 28,
                Location = new LocationDb() { Latitude = 51.58883, Longitude = 4.77617 },
                Name = "Aanwijzing",
                Description = "Kruising Grote Markt / Stadserf (je gaat weer terug)",

            });
            sights.Add(new Sight()
            {
                Sequence = 29,
                Location = new LocationDb() { Latitude = 51.58747, Longitude = 4.77662 },
                Name = "Aanwijzing",
                Description = "Terug naar begin Grote Markt",

            });
            sights.Add(new Sight()
            {
                Sequence = 30,
                Location = new LocationDb() { Latitude = 51.58761, Longitude = 4.77712 },
                Name = "Antonius van Paduakerk",
                Description = "",

            });
            sights.Add(new Sight()
            {
                Sequence = 31,
                Location = new LocationDb() { Latitude = 51.58828, Longitude = 4.77858 },
                Name = "Aanwijzing",
                Description = "Kruising St. Jansstraat / Molenstraat",

            });
            sights.Add(new Sight()
            {
                Sequence = 32,
                Location = new LocationDb() { Latitude = 51.58773, Longitude = 4.77948 },
                Name = "Bibliotheek",
                Description = "",

            });
            sights.Add(new Sight()
            {
                Sequence = 33,
                Location = new LocationDb() { Latitude = 51.58752, Longitude = 4.77994 },
                Name = "Aanwijzing",
                Description = "Kruising Molenstraat / Kloosterplein",

            });
            sights.Add(new Sight()
            {
                Sequence = 34,
                Location = new LocationDb() { Latitude = 51.58794, Longitude = 4.78105 },
                Name = "Kloosterkazerne",
                Description = "1e bocht Kloosterplein",

            });
            sights.Add(new Sight()
            {

                Sequence = 35,
                Location = new LocationDb() { Latitude = 51.58794, Longitude = 4.78218 },
                Name = "Chasse theater",
                Description = "2e bocht Kloosterplein",

            });
            sights.Add(new Sight()
            {
                Sequence = 36,
                Location = new LocationDb() { Latitude = 51.58794, Longitude = 4.78105 },
                Name = "Aanwijzing",
                Description = "1e bocht Kloosterplein / Begin Vlaszak",

            });

            sights.Add(new Sight()
            {
                Sequence = 37,
                Location = new LocationDb() { Latitude = 51.58955, Longitude = 4.78038 },
                Name = "Aanwijzing",
                Description = "Einde Vlaszak / Begin Boschstraat",

            });
            sights.Add(new Sight()
            {
                Sequence = 38,
                Location = new LocationDb() { Latitude = 51.58966, Longitude = 4.78076 },
                Name = "Beyerd",
                Description = "",

            });
            sights.Add(new Sight()
            {
                Sequence = 39,
                Location = new LocationDb() { Latitude = 51.58939, Longitude = 4.77982 },
                Name = "Gasthuispoort",
                Description = "1e bocht Catharinastraat / Veemarktstraat",

            });
            sights.Add(new Sight()
            {
                Sequence = 40,
                Location = new LocationDb() { Latitude = 51.58905, Longitude = 4.77981 },
                Name = "Aanwijzing",
                Description = "2e bocht Veemarktstraat",

            });
            sights.Add(new Sight()
            {
                Sequence = 41,
                Location = new LocationDb() { Latitude = 51.58846, Longitude = 4.77830 },
                Name = "Aanwijzing",
                Description = "Kruising St. Annastraat / Veemarktstraat",

            });
            sights.Add(new Sight()
            {
                Sequence = 42,
                Location = new LocationDb() { Latitude = 51.58905, Longitude = 4.77801 },
                Name = "Willem Merkxtuin",
                Description = "De ingang",

            });
            sights.Add(new Sight()
            {
                Sequence = 43,
                Location = new LocationDb() { Latitude = 51.58918, Longitude = 4.77841 },
                Name = "Aanwijzing",
                Description = "Binnen Willem Merkxtuin",

            });
            sights.Add(new Sight()
            {
                Sequence = 44,
                Location = new LocationDb() { Latitude = 51.58905, Longitude = 4.77801 },
                Name = "Aanwijzing",
                Description = "Uitgang Willem Merkxtuin",

            });
            sights.Add(new Sight()
            {
                Sequence = 45,
                Location = new LocationDb() { Latitude = 51.58960, Longitude = 4.77770 },
                Name = "Aanwijzing",
                Description = "Kruising Catharinastraat / St. Annastraat",

            });
            sights.Add(new Sight()
            {
                Sequence = 46,
                Location = new LocationDb() { Latitude = 51.58965, Longitude = 4.77830 },
                Name = "Begijnenhof",
                Description = "De ingang",

            });
            sights.Add(new Sight()
            {
                Sequence = 47,
                Location = new LocationDb() { Latitude = 51.58997, Longitude = 4.77810 },
                Name = "Aanwijzing",
                Description = "Binnen Begijnenhof",

            });
            sights.Add(new Sight()
            {
                Sequence = 48,
                Location = new LocationDb() { Latitude = 51.58965, Longitude = 4.77830 },
                Name = "Aanwijzing",
                Description = "Uitgang Begijnenhof",

            });

            sights.Add(new Sight()
            {
                Sequence = 49,
                Location = new LocationDb() { Latitude = 51.58950, Longitude = 4.77649 },
                Name = "Eindpunt stadswandeling",
                Description = "Eindpunt",

            });
            return sights;
        }

        private List<Sight> generateSightShort()
        {
            int sequence = 0;
            List<Sight> sights = new List<Sight>();
            sights.Add(new Sight()
            {
                Sequence = sequence++,
                Location = new LocationDb() { Latitude = 51.59380, Longitude = 4.77963 },
                Name = "VVV Breda",
                ImagePath = new String[]{"vvvkantoor.jpg"},                
            });
            sights.Add(new Sight()
            {
                Sequence = sequence++,
                Location = new LocationDb() { Latitude = 51.59307, Longitude = 4.77969 },
                Name = "Liefdeszuster",
                ImagePath = new String[]{"liefdeszuster.jpg"},
                BuildYear = new DateTime(1990, 8, 10)
            });
            sights.Add(new Sight()
            {
                Sequence = sequence++,
                Location = new LocationDb() { Latitude = 51.59266, Longitude = 4.77972 },
                Name = "Nassau-Baroniemonument",
                ImagePath = new String[] { "nassaubaroniemonument1.jpg", "nassaubaroniemonument2.jpg" },
                BuildYear = new DateTime(1905, 6, 3)
            });
            sights.Add(new Sight()
            {
                Sequence = sequence++,
                Location = new LocationDb() { Latitude = 51.59221, Longitude = 4.77998 }, 	
                Name = "Valkenberg",
                ImagePath = new String[] { "valkenberg1.jpg", "valkenberg2.jpg", "valkenberg3.jpg" },
            });

            sights.Add(new Sight()
            {
                Sequence = sequence++,
                Location = new LocationDb() { Latitude = 51.59274, Longitude = 4.77867 },
                Name = "Vuurtoren",
                ImagePath = new String[] { "vuurtoren1.jpg" },
            });

            sights.Add(new Sight()
            {
                Sequence = sequence++,
                Location = new LocationDb() { Latitude = 51.59125, Longitude = 4.77751 },  	
                Name = "Kasteel van Breda",
                BuildYear = new DateTime(1350, 1, 1),
                ImagePath = new String[] { "kasteel1.jpg" },
            });
            sights.Add(new Sight()
            {
                Sequence = sequence++,
                Location = new LocationDb() { Latitude = 51.59038, Longitude = 4.77625 },  	
                Name = "Stadhouderspoort",
                ImagePath = new String[] { "stadshouderspoort1.jpg", "stadshouderspoort2.jpg" },
            });            
            sights.Add(new Sight()
            {
                Sequence = sequence++,
                Location = new LocationDb() { Latitude = 51.59043, Longitude = 4.77518 },
                Name = "Huis van Brecht",
                BuildYear = new DateTime(1490, 1, 1),
                ImagePath = new String[] { "huisbrecht1.jpg", "huisbrecht2.jpg"},
            });
            sights.Add(new Sight()
            {
                Sequence = sequence++,
                Location = new LocationDb() { Latitude = 51.59010, Longitude = 4.77336 },
                Name = "Spanjaardsgat",
                ImagePath = new String[] { "spanjaardsgat1.jpg", "spanjaardsgat2.jpg", "spanjaardsgat3.jpg" },
                BuildYear = new DateTime(1509, 1, 1)
            });
            sights.Add(new Sight()
            {
                Sequence = sequence++,
                Location = new LocationDb() { Latitude = 51.58982, Longitude = 4.77321 },
                Name = "Vismarkt",
                BuildYear = new DateTime(1367, 1, 1),
                ImagePath = new String[] { "vismarkt1.jpg" },
            });
            sights.Add(new Sight()
            {
                Sequence = sequence++,
                Location = new LocationDb() { Latitude = 51.58932, Longitude = 4.77444 },
                Name = "Havermarkt",
                BuildYear = new DateTime(1490, 1, 1),
                ImagePath = new String[] { "havermarkt1.jpg", "havermarkt2.jpg" },
            });
            
            sights.Add(new Sight()
            {
                Sequence = sequence++,
                Location = new LocationDb() { Latitude = 51.58878, Longitude = 4.77549 },
                Name = "Grote Kerk",
                BuildYear = new DateTime(1410, 1, 1),
                ImagePath = new String[] { "grote_kerk1.jpg", "grote_kerk2.jpg", "grote_kerk3.jpg", "grote_kerk4.jpg", "grote_kerk5.jpg" },
            });
            //sights.Add(new Sight()
            //{
            //    Sequence = sequence++,
            //    Location = new LocationDb() { Latitude = 51.58822, Longitude = 4.77525 },
            //    Name = "Het poortje",
            //    ImagePath = new String[] { "poortje1.jpg" },
            //});
            sights.Add(new Sight()
            {
                Sequence = sequence++,
                Location = new LocationDb() { Latitude = 51.58716, Longitude = 4.77582 },
                Name = "Ridderstraat",
                ImagePath = new String[] { "ridderstraat1.jpg", "ridderstraat2.jpg", "ridderstraat3.jpg", "ridderstraat4.jpg" },

            });
            sights.Add(new Sight()
            {
                Sequence = sequence++,
                Location = new LocationDb() { Latitude = 51.58747, Longitude = 4.77662 },
                Name = "Grote Markt",
                ImagePath = new String[] { "grote_markt1.jpg", "grote_markt2.jpg" },
            });
            sights.Add(new Sight()
            {
                Sequence = sequence++,
                Location = new LocationDb() { Latitude = 51.58771, Longitude = 4.77652 },
                Name = "Het Wit Lam",
                ImagePath = new String[] { "witlam1.jpg", "witlam2.jpg", "witlam3.jpg" },

            });
            sights.Add(new Sight()
            {
                Sequence = sequence++,
                Location = new LocationDb() { Latitude = 51.58797, Longitude = 4.77638 },
                Name = "Bevrijdingsmonument",
                ImagePath = new String[] { "bevrijdingsmonument1.jpg", "bevrijdingsmonument2.jpg", "bevrijdingsmonument3.jpg" },
            });
            sights.Add(new Sight()
            {
                Sequence = sequence++,
                Location = new LocationDb() { Latitude = 51.58885, Longitude = 4.77616 },
                Name = "Stadshuis",
                ImagePath = new String[] { "stadshuis1.jpg", "stadshuis2.jpg" },

            });              
            sights.Add(new Sight()
            {
                Sequence = sequence++,
                Location = new LocationDb() { Latitude = 51.58761, Longitude = 4.77712 },
                Name = "Antonius Kathedraal",
                ImagePath = new String[] { "antonius1.jpg", "antonius2.jpg", "antonius3.jpg" },

            });
            sights.Add(new Sight()
            {
                Sequence = sequence++,
                Location = new LocationDb() { Latitude = 51.58773, Longitude = 4.77948 },
                Name = "Bibliotheek",
                ImagePath = new String[] { "library1.jpg", "library2.jpg" },

            });
            sights.Add(new Sight()
            {
                Sequence = sequence++,
                Location = new LocationDb() { Latitude = 51.58794, Longitude = 4.78105 },
                Name = "Kloosterkazerne",
                ImagePath = new String[] { "kloosterkazerne1.jpg" },
                BuildYear = new DateTime(1504, 1, 1),

            });
            sights.Add(new Sight()
            {
                Sequence = sequence++,
                Location = new LocationDb() { Latitude = 51.58794, Longitude = 4.78218 },
                Name = "Chasse theater",
                ImagePath = new String[] { "chasse1.jpg"},

            });
            sights.Add(new Sight()
            {
                Sequence = sequence++,
                Location = new LocationDb() { Latitude = 51.58859, Longitude = 4.78068 },
                Name = "Binding van Isaac",
                ImagePath = new String[] { "isaac1.jpg", "isaac2.jpg" },

            });
            sights.Add(new Sight()
            {
                Sequence = sequence++,
                Location = new LocationDb() { Latitude = 51.58966, Longitude = 4.78076 },
                Name = "Beyerd",
                ImagePath = new String[] { "beyerd1.jpg", "beyerd2.jpg"},
            });
            sights.Add(new Sight()
            {
                Sequence = sequence++,
                Location = new LocationDb() { Latitude = 51.58939, Longitude = 4.77982 },
                Name = "Gasthuispoort",
                ImagePath = new String[] { "gasthuispoort1.jpg", "gasthuispoort2.jpg"},

            });
            sights.Add(new Sight()
            {
                Sequence = sequence++,
                Location = new LocationDb() { Latitude = 51.58905, Longitude = 4.77801 },
                Name = "Willem Merkxtuin",
                ImagePath = new String[] { "merkxtuin1.jpg", "merkxtuin2.jpg", "merkxtuin3.jpg", "merkxtuin4.jpg" },

            });
            sights.Add(new Sight()
            {
                Sequence = sequence++,
                Location = new LocationDb() { Latitude = 51.58965, Longitude = 4.77830 },
                Name = "Begijnenhof",
                ImagePath = new String[] { "begijnenhof1.jpg", "begijnenhof2.jpg" },

            });
            sights.Add(new Sight()
            {
                Sequence = sequence++,
                Location = new LocationDb() { Latitude = 51.58950, Longitude = 4.77649 },
                Name = "Eindpunt stadswandeling",
                ImagePath = new String[] { "the_end.jpg" },
            });
            return sights;
        }
    }
}
