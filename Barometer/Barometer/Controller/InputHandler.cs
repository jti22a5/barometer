﻿using Barometer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;


namespace Barometer.Controller
{
    /// <summary>
    /// De invoer controller
    /// </summary>
    public class InputHandler
    {
        /// <summary>
        /// De GPS invoer
        /// </summary>
        public Model.GPSInput gpsInput;

        /// <summary>
        /// De user invoer
        /// </summary>
        private Model.UserInput userInput;
        public delegate void LocationTriggerHandler(Sight sight);
        public event LocationTriggerHandler LocationTriggeredEvent;

        /// <summary>
        /// Initialiseert de input handler
        /// </summary>
        public void Initialize()
        {
            gpsInput = new Model.GPSInput();
            gpsInput.Initialize();
            userInput = new Model.UserInput();

            gpsInput.GetGeolocator().PositionChanged += new Windows.Foundation.TypedEventHandler<Geolocator, PositionChangedEventArgs>(positionChangedEvent);
        }

        /// <summary>
        /// Dit event wordt wordt uitgevoerd zodra het PositionChanged event in de geolocator wordt aangeroepen. De geolocator zit in de model class GPSInput.
        /// Momenteel wordt gekeken of de geolocation zich binnen de coordinaten van de stad Breda zijn.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public void positionChangedEvent(Geolocator sender, PositionChangedEventArgs args)
        {
            if (args.Position.Coordinate.Point.Position.Latitude < 51.59550 && args.Position.Coordinate.Point.Position.Latitude > 51.58447)
            {
                if (args.Position.Coordinate.Point.Position.Longitude > 4.75973 && args.Position.Coordinate.Point.Position.Longitude < 4.78603)
                {                    
                    Controller.Barometer.getGUIController().OutOfBoundsFlag = false;
                    checkLocationTrigger(args.Position.Coordinate.Point.Position);
                }
            }
            else
            {
                if (!Controller.Barometer.getGUIController().OutOfBoundsFlag)
                {
                    Controller.Barometer.getGUIController().OutOfBoundsFlag = true;
                }
            }
            Barometer.getGUIController().drawLocationPosition(sender, args);
        }

        public void checkLocationTrigger(BasicGeoposition currentPosition)
        {
            if(Controller.Barometer.getSession().Route != null && Controller.Barometer.MapReadyFlag)
            {
                foreach(Sight sight in Controller.Barometer.getSession().Route.Sights)
                {
                    if (sight.Location.withinBounds(currentPosition))
                    {
                        LocationTriggeredEvent(sight);                            
                    }               
                }
            }
        }

        public async void RequestGPSInput()
        {
            await gpsInput.ProcessCurrentLocation();
            gpsInput.GetCurrentLocation();
        }
    }
}
