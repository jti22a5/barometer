﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Barometer.Controller
{
    class BatteryController
    {
        public static bool MessageFlag = false;

        /// <summary>
        /// AC line status
        /// </summary>
        public enum ACLineStatus : byte
        {
            Offline = 0, 
            Online = 1,
            Unknown = 255
        }

        /// <summary>
        /// Batteryflag
        /// </summary>
        public enum BatteryFlag : byte
        {
            High = 1,
            Low = 2,
            Critical = 4,
            Charging = 8,
            NoSystemBattery = 128,
            Unknown = 255
        }

        /// <summary>
        /// The struct copied from MSDN
        /// http://msdn.microsoft.com/en-us/library/windows/desktop/aa373232%28v=vs.85%29.aspx
        /// </summary>
        [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential)]
        public class SystemPowerStatus
        {
            public ACLineStatus ACLineStatus;
            public BatteryFlag BatteryFlag;
            public Byte BatteryLifePercent;
            public Byte Reserved1;
            public Int32 BatteryLifeTime;
            public Int32 BatteryFullLifeTime;
        }

        /// <summary>
        /// http://msdn.microsoft.com/en-us/library/windows/desktop/aa372693%28v=vs.85%29.aspx
        /// </summary>
        /// <param name="pSystemPowerStatus"></param>
        /// <returns></returns>
        [System.Runtime.InteropServices.DllImport("kernel32.dll")]
        private static extern bool GetSystemPowerStatus(SystemPowerStatus pSystemPowerStatus);
        
        /// <summary>
        /// Returns battery level
        /// </summary>
        /// <returns></returns>
        public static int getBatteryLevel()
        {
            SystemPowerStatus pSystemPowerStatus = new SystemPowerStatus();
            GetSystemPowerStatus(pSystemPowerStatus);
            return (int)pSystemPowerStatus.BatteryLifePercent;
        }
    }
}
