﻿using Barometer.Model;
using System;
using SQLite;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Popups;
using Windows.Devices.Geolocation;

namespace Barometer.Controller
{
    class Barometer
    {
        private static Session session;
        private static GUIController guiController;
        private static BingMapsHandler bingMapsHandler;
        private static InputHandler inputHandler;
        private static DatabaseHandler databaseHandler;
        public static bool MapReadyFlag = false;

        public static bool initialize()
        {
            guiController = new GUIController();
            inputHandler = new InputHandler();
            databaseHandler = new DatabaseHandler();
            setSession();                      

            bingMapsHandler = new BingMapsHandler();            
            return true;
        }

        public static GUIController getGUIController()
        {
            return guiController;
        }

        public static BingMapsHandler getBingMapsHandler()
        {
            return bingMapsHandler;
        }

        public static DatabaseHandler getDatabaseHandler()
        {
            return databaseHandler;
        }

        public static InputHandler getInputHandler()
        {
            return inputHandler;
        }

        public static Session getSession()
        {
            return session;
        }

        public static void setSession()
        {
            session = new Session()
            {
                Route = null,
                Context = new Windows.ApplicationModel.Resources.Core.ResourceContext(),
                RMap = Windows.ApplicationModel.Resources.Core.ResourceManager.Current.MainResourceMap.GetSubtree("Resources")
            };
            session.Context.Languages = new string[] { Controller.Barometer.getGUIController().languages[(0)] }; // 0 is nl-NL by default
        }

        public static void setSession(Session s)
        {
            session = s;
            Controller.Barometer.getBingMapsHandler().Initialize();
            Controller.Barometer.getGUIController().navigateToPage(typeof(View.Main));
        }

        public async static void loadDataFromSavedSession()
        {
            Session pSession = await Controller.Barometer.getDatabaseHandler().LoadSession();

            // Re-add language stuff
            pSession.RMap = Windows.ApplicationModel.Resources.Core.ResourceManager.Current.MainResourceMap.GetSubtree("Resources");
            pSession.Context = new Windows.ApplicationModel.Resources.Core.ResourceContext();

            Controller.Barometer.setSession(pSession);
        }

        public static async void ShowError(string title, string description)
        {
            try
            {
                var messageDialog = new MessageDialog(description, title);
                await messageDialog.ShowAsync();
            }
            catch { }
        }

        public static Uri baseUri = new Uri("ms-appx:///");
    }
}
