﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bing.Maps;
using SQLite;
using System.Runtime.Serialization;

namespace Barometer.Model
{
    /// <summary>
    /// Deze klasse bevat alle informatie over een ingestelde route.
    /// </summary>
    
    [DataContract]
    public class Route
    {
        /// <summary>
        /// Een unieke ID gekoppeld aan de route
        /// </summary>
        [PrimaryKey, AutoIncrement, DataMember()]
        public int ID { get; set; }
        /// <summary>
        /// De naam van de route
        /// </summary>
        [DataMember()]
        public string Name { get; set; }
        /// <summary>
        /// Een lijst met bezienswaardigheden die te zien zijn op de route
        /// </summary>
        
        [Ignore, DataMember()]
        public List<Sight> Sights { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
