﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Barometer.Model
{
    public class RouteSightDB
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

        public int RouteId { get; set; }
        public int SightId { get; set; }
    }
}
