﻿using Bing.Maps;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.UI.Popups;

namespace Barometer.Model
{
    /// <summary>
    /// Een sessie is gekoppeled aan de gebruiker. Een sessie bevat een route, de werkelijk gelopen route en gebruikers instellingen.
    /// </summary>
    [DataContract]
    public class Session
    {
        [PrimaryKey, AutoIncrement, DataMember()]
        public int ID { get; set; }
        /// <summary>
        /// Bevat de route die in de sessie is opgeslagen. Deze Route wordt aangemaakt nadat de route is opgehaald uit de database.
        /// </summary>
        [Ignore, DataMember()]
        public Route Route { get; set; }
        /// <summary>
        /// De walked lijst property bevat een lijst met locaties waar de gebruiker is geweest. dit wordt gebruikt om de werkelijk getekende route te tekenen.
        /// </summary>        
        [Ignore, DataMember()]
        public List<LocationDb> Walked { get; set; }
        /// <summary>
        /// Bevat de instellingen die de gebruiker heeft gezet in de applicatie.
        /// </summary>
        [Ignore, DataMember()]
        public Language Language { get; set; }        
        
        [DataMember()]
        public DateTime CreateTime { get; set; }

        public Windows.ApplicationModel.Resources.Core.ResourceMap RMap { get; set; }

        public Windows.ApplicationModel.Resources.Core.ResourceContext Context { get; set; }

        public void changeRoute(Route newRoute)
        {
            Route = newRoute;
            if (Controller.Barometer.getGUIController().getMainPage() != null)
            {
                Controller.Barometer.getGUIController().getMainPage().setRouteName(newRoute.Name);
                Controller.Barometer.getBingMapsHandler().Initialize();
            }
            else
            {
                Controller.Barometer.getGUIController().navigateToPage(typeof(View.Main));
            }
        }

        //Moet van de database
        public int RouteId { get; set; }
        public int LanguageId { get; set; } //Dat is het enige wat in settings sta.


        /// <summary>
        /// voegt een nieuwe locatie toe aan de walked lijst, zodat er opnieuw getekend kan worden.
        /// </summary>
        /// <param name="loc">De laatste nieuwe locatie van de gebruiker</param>
        public void AddWalked(Location loc)
        {

        }
    }
}
