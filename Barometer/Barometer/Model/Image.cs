﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Barometer.Model
{
    /// <summary>
    /// Verwerkt een afbeelding van een bezienswaardigheid
    /// </summary>
    public class Image : Media
    {
        /// <summary>
        /// The actual bitmap image
        /// </summary>
        public Windows.UI.Xaml.Media.Imaging.BitmapImage SourceImage { get; set; }
    }
}
