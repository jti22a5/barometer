﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;
using Windows.UI.Xaml;

namespace Barometer.Model
{
    /// <summary>
    /// Verwerkt de gebruikersinvoer
    /// </summary>
    class UserInput
    {
        /// <summary>
        /// Initialiseert het gebruik van deze klasse
        /// </summary>
        public void Initialize()
        {
            // nothing...
        }

        /// <summary>
        /// Triggered when the user hits the current route button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void btnCurrRoute_Click(object sender, RoutedEventArgs e)
        {
            Controller.Barometer.getGUIController().navigateToPage(typeof(View.SelectRoute));
        }

        /// <summary>
        /// Triggered when the user hits the language button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void btnLanguage_Click(object sender, RoutedEventArgs e)
        {
            Controller.Barometer.getGUIController().setLanguage();             
        }

        /// <summary>
        /// Triggered when the user hits the stop route button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void btnStopRoute_Click(object sender, RoutedEventArgs e)
        {
            if(Barometer.Controller.GUIController.routeFlag)
            {
                Controller.Barometer.getBingMapsHandler().Clear();
                Barometer.Controller.GUIController.routeFlag = false;
            }
            else
            {
                Controller.Barometer.getGUIController().navigateToPage(typeof(View.SelectRoute));
                Barometer.Controller.GUIController.routeFlag = true;
            }
            Controller.Barometer.getGUIController().setStopButton(Barometer.Controller.GUIController.routeFlag);
        }

        /// <summary>
        /// Triggered when the user hits the settings button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void btnSettings_Click(object sender, RoutedEventArgs e)
        {
            Controller.Barometer.getGUIController().showSettingsMenu();
        }

        /// <summary>
        /// Triggered when the user hits the help button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void btnHelp_Click(object sender, RoutedEventArgs e)
        {
            Controller.Barometer.getGUIController().showTutorial();
            //BasicGeoposition atlas = new BasicGeoposition();
            //atlas.Latitude = 51.46995;
            //atlas.Longitude = 5.64842;
            //Controller.Barometer.getInputHandler().checkLocationTrigger(atlas);
        }

        /// <summary>
        /// Triggered when the user hits the login button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            Controller.Barometer.getGUIController().toggleSettingsButton();
        }

        /// <summary>
        /// Triggered when the user hits the save button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void btnSaveSession_Click(object sender, RoutedEventArgs e)
        {
            Barometer.Controller.Barometer.getDatabaseHandler().SaveSession(Barometer.Controller.Barometer.getSession());
        }

        /// <summary>
        /// Triggered when the user loads a session
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public async static void btnLoadSession_Click(object sender, RoutedEventArgs e)
        {
            Controller.Barometer.loadDataFromSavedSession();
        }
    }
}
