﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

namespace Barometer.Model
{
    /// <summary>
    /// De bezienswaardigheid
    /// </summary>
    [DataContract]
    public class Sight
    {
        /// <summary>
        /// Elke bezienswaardigheid heeft een uniek ID om zich te onderscheiden
        /// </summary>
        [PrimaryKey, AutoIncrement, DataMember()]
        public int ID { get; set; }
        [DataMember()]
        public int Sequence { get; set; }

        /// <summary>
        /// Elke bezienswaardigheid heeft een naam
        /// </summary>
        [DataMember()]
        public String Name { get; set; }

        /// <summary>
        /// De bezienswaardigheid kan verschillende statussen bezitten: 'bezocht, gezien en niet gezien'
        /// </summary>
        [DataContract(Name = "EnumState")]
        public enum EnumState 
        {
            [EnumMember]
            Visited,
            [EnumMember]
            Viewed,
            [EnumMember]
            Unviewed 
        };

        /// <summary>
        /// The actual state of the sight
        /// </summary>
        [DataMember()]
        public EnumState State { get; set; }

        /// <summary>
        /// Elke bezienswaardigheid bevat een beschrijving
        /// </summary>
        [DataMember()]
        public String Description { get; set; }

        /// <summary>
        /// De openingstijd van de bezienswaardigheid wordt hierin opgeslagen
        /// </summary>
        [DataMember()]
        public String OpeningDescription { get; set; }

        /// <summary>
        /// De positie van de bezienswaardigheid wordt hierin opgeslagen
        /// </summary>
        [Ignore, DataMember()]
        public LocationDb Location
        {
            get;
            set;
        }

        /// <summary>
        /// Het jaar waarin de bezienswaardigheid gebouwd is wordt hierin opgeslagen
        /// </summary>
        [DataMember()]
        public DateTime BuildYear { get; set; }



        /// <summary>
        /// Type of media
        /// </summary>
        public enum MediaEnum
        {
            MEDIA_AUDIO,
            MEDIA_IMAGE
        }

        /// <summary>
        /// Eventueel beschikbare media wordt hierin opgeslagen
        /// </summary>
        [Ignore]
        public Dictionary<MediaEnum, Media> Media { get; set; }

        [DataMember()]
        public String[] ImagePath;


        public ImageSource[] Image
        {
            get
            {
                if (ImagePath != null)
                {
                    BitmapImage[] array = new BitmapImage[ImagePath.Length];
                    for (int i = 0; i < ImagePath.Length; i++)
                    {
                        array[i] = new BitmapImage(new Uri(Controller.Barometer.baseUri, "Images/" + this.ImagePath[i]));
                    }
                    return array;
                }
                else
                {
                    return null;
                }
            }
        }

        [Obsolete]
        public void addHardcodedImage(string strImage)
        {
            Media.Add(Sight.MediaEnum.MEDIA_IMAGE, new Model.Image()
            {
                SourceImage = new Windows.UI.Xaml.Media.Imaging.BitmapImage()
                {
                    UriSource = new Uri("ms-appx:///Assets/Sight Images/" + strImage)
                }
            });
        }
    }
}
