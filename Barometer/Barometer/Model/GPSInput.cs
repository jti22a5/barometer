﻿using Bing.Maps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;
using Windows.UI.Core;
using Windows.UI.Xaml;

namespace Barometer.Model
{
    /// <summary>
    /// Verwerkt de GPS invoer
    /// </summary>
    public class GPSInput
    {
        private Geolocator geolocator;
        private CancellationTokenSource cts;
        private CancellationToken token;
        public Geoposition position;
        private DispatcherTimer timer;
        private Geoposition lastPosition;

        /// <summary>
        /// Initialiseert het gebruik van deze klasse
        /// </summary>
        public void Initialize()
        {
            geolocator = new Geolocator();
            geolocator.DesiredAccuracy = PositionAccuracy.High;
            geolocator.ReportInterval = 500;
            timer = new DispatcherTimer();
            timer.Start();            
        }

        /// <summary>
        /// Geeft de huidige GPS positie
        /// </summary>
        /// <returns>GPS position of null indien er een fout optreedt</returns>
        public async Task ProcessCurrentLocation()
        {
            try
            {
                Windows.Foundation.IAsyncOperation<Geoposition> locationTask = null;
                try
                {
                    Geoposition tempPosition = await geolocator.GetGeopositionAsync(); // TimeSpan.FromMinutes(1), TimeSpan.FromSeconds(3);
                        
                    Location tempTopLeft = new Location(51.612021, 4.693973);
                    Location tempBottomRight = new Location(51.54878, 4.846151);

                    bool tempInBreda = tempPosition.Coordinate.Point.Position.Latitude > tempBottomRight.Latitude && tempPosition.Coordinate.Point.Position.Latitude < tempTopLeft.Latitude &&
                                       tempPosition.Coordinate.Point.Position.Longitude > tempTopLeft.Longitude && tempPosition.Coordinate.Point.Position.Longitude < tempBottomRight.Longitude;

                    
                    if (lastPosition != null && tempInBreda)
                    {
                        this.position = lastPosition;
                    } 
                    else if(tempInBreda)
                    {
                        this.position = tempPosition;
                    }
                    //Er is geen eerdere positie opgeslagen en de gebruiker bevindt zich volgens de GPS-coördinaten buiten Breda
                    else
                    {

                    }

                    if (position != null)
                    {
                        lastPosition = position;
                    }
                }
                catch (Exception)
                {
                    
                }
                finally
                {
                    if (locationTask != null)
                    {
                        if (locationTask.Status == Windows.Foundation.AsyncStatus.Started)
                            locationTask.Cancel();

                        locationTask.Close();
                    }
                }
            }
            catch(Exception)
            { }          
        }

        public Location GetCurrentLocation()
        {
            return new Location(position.Coordinate.Latitude, position.Coordinate.Longitude);            
        }

        /// <summary>
        /// Vraagt de geolocator op uit de model class
        /// </summary>
        /// <returns>De huidige geolocator</returns>
        public Geolocator GetGeolocator()
        {
            return geolocator;
        }
    }
}
