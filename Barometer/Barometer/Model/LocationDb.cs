﻿using Bing.Maps;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Barometer.Model
{
    [DataContract]
    public class LocationDb
    {
        [PrimaryKey, DataMember()]
        public int SightId { set; get; }
        [DataMember()]
        public double Latitude { set; get; }
        [DataMember()]
        public double Longitude { set; get; }
        private double boundLimit = 0.0002; // 0.0001

        public Location getBingMapLocation()
        {
            return new Location(this.Latitude, this.Longitude);
        }
        
        /// <summary>
        /// Used for custom geofencing, this returns a top left offset of the original location 
        /// </summary>
        /// <returns></returns>
        public Location getTopLeftLocation()
        {
            if(Latitude != null && Longitude != null)
            {
                return new Location(Latitude + boundLimit, Longitude - boundLimit);
            }            
            return null;            
        }

        /// <summary>
        /// Used for custom geofencing, this returns a down right offset of the original location 
        /// </summary>
        /// <returns></returns>
        public Location getDownRightLocation()
        {
            if (Latitude != null && Longitude != null)
            {
                return new Location(Latitude - boundLimit, Longitude + boundLimit);
            }
            return null;
        }       

        /// <summary>
        /// Checks if the given Longitude and Latidude are within the bounds.
        /// Uses 4 nested if statements because of thread safety issues, unresolved as of now.
        /// </summary>
        /// <param name="currentPosition"></param>
        /// <returns></returns>
        public bool withinBounds(Windows.Devices.Geolocation.BasicGeoposition currentPosition)
        {
            if(Latitude != null && Longitude != null)
            {   
                if(currentPosition.Latitude <= Latitude + boundLimit)
                {
                    if(currentPosition.Latitude >= Latitude - boundLimit)
                    {
                        if(currentPosition.Longitude <= Longitude + boundLimit)
                        {
                            if(currentPosition.Longitude >= Longitude - boundLimit)
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }
    }
}
