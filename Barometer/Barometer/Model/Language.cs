﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace Barometer.Model
{
    /// <summary>
    /// Hierin staat een taal
    /// </summary>
    [DataContract]
    public class Language
    {
        /// <summary>
        /// Dit is een unieke id deze wordt gebruikt om deze taal uniek in de database weer te geven.
        /// </summary>
       [DataMember()]
        public int ID
        {
            private set;
            get;
        }

        /// <summary>
        /// Dit is de naam van de taal dit zal bijvoorbeeld: Nederlands kunnen zijn.
        /// </summary>
        [DataMember()]
        public string Name
        {
            set;
            get;
        }
        /// <summary>
        /// Dit is een afbeelding van de vlag.
        /// </summary>
        [DataMember()]
        public Image Flag
        {
            set;
            get;
        }

    }
}
