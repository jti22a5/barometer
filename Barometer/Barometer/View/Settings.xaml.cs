﻿using Barometer.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Settings Flyout item template is documented at http://go.microsoft.com/fwlink/?LinkId=273769

namespace Barometer.View
{
    public sealed partial class Settings : SettingsFlyout
    {
        /// <summary>
        /// The selected storagefile (image)
        /// </summary>
        private StorageFile m_pStorageFile;

        /// <summary>
        /// Constructor
        /// </summary>
        public Settings()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// On image button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnImage_Click(object sender, RoutedEventArgs e)
        {
            FileOpenPicker pFileOpenPicker = new FileOpenPicker();
            pFileOpenPicker.ViewMode = PickerViewMode.Thumbnail;
            pFileOpenPicker.SuggestedStartLocation = PickerLocationId.ComputerFolder;
            pFileOpenPicker.FileTypeFilter.Add(".jpg");
            pFileOpenPicker.FileTypeFilter.Add(".jpeg");
            pFileOpenPicker.FileTypeFilter.Add(".png");
            StorageFile pStorageFile = await pFileOpenPicker.PickSingleFileAsync();

            if (pStorageFile != null)
            {
                Barometer.Controller.Barometer.ShowError("Attention", pStorageFile.Name);   
            }
        }

        /// <summary>
        /// On add button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            double dbLat = 0, dbLong = 0;

            double.TryParse(txtLatitude.Text, out dbLat);
            double.TryParse(txtLongtitude.Text, out dbLong);

            bool bSuccess = Controller.Barometer.getDatabaseHandler().createSightWithRoute(
                new Sight()
                {
                    Name = txtName.Text,
                    Location = new LocationDb() { Latitude = dbLat, Longitude = dbLong },
                    Description = txtDesc.Text,
                    Media = new Dictionary<Model.Sight.MediaEnum, Model.Media>()
                }, Controller.Barometer.getSession().Route);

            if (bSuccess)
            {
                Controller.Barometer.getSession().changeRoute(Controller.Barometer.getSession().Route);
            }

        }
    }
}
