﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Barometer.View
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SelectRoute : Page
    {
        public SelectRoute()
        {
            this.InitializeComponent();
            btnSelectRoute.Content = Controller.Barometer.getSession().RMap.GetValue("selectRoute", Controller.Barometer.getSession().Context).ValueAsString;


            /*Controller.Barometer.getDatabaseHandler().createRoute(new Model.Route()
            {
                Name = "Maarten's route",
                Sights = new List<Model.Sight>()
                {
                    new Model.Sight() 
                    {
                        Name = "Avans Lovensdijkstraat 61",
                        Description = "Leerzaam",
                        OpeningDescription = "Werkdagen",
                        Location = new Barometer.Model.LocationDb()
                        {
                            Latitude = 51.58578,
                            Longitude = 4.79323,
                        },
                        BuildYear = new DateTime(2012, 06, 10),
                        State = Barometer.Model.Sight.EnumState.Viewed
                    },
                    new Model.Sight() 
                    {
                        Name = "Oostmolenweg 70",
                        Description = "Niet leerzaam",
                        OpeningDescription = "ALTIJD",
                        Location = new Barometer.Model.LocationDb()
                        {
                            Latitude = 51.490389,
                            Longitude = 3.910807,
                        },
                        BuildYear = new DateTime(2012, 06, 10),
                        State = Barometer.Model.Sight.EnumState.Viewed
                    }
                }
            });*/

            if (Controller.Barometer.getDatabaseHandler().GetRoutes() != null && Controller.Barometer.getDatabaseHandler().GetRoutes().Count != 0)
            {
                foreach (Model.Route pRoute in Controller.Barometer.getDatabaseHandler().GetRoutes()) 
                {
                    lstRoutes.Items.Add(pRoute);
                }
            }
            else
            {
                lstRoutes.Items.Add(new Model.Route()
                {
                    Name = "Historische Kilometer Breda (hc)"
                });
            }
            
        }

        private void btnSelectRoute_Click(object sender, RoutedEventArgs e)
        {
            if (lstRoutes.SelectedItem == null) {
                return;
            }
            if (((Model.Route)lstRoutes.SelectedItem).Sights != null)
            {
                Controller.Barometer.getSession().changeRoute((Model.Route)lstRoutes.SelectedItem);
            }            
            Controller.Barometer.getGUIController().navigateToPage(typeof(View.Main));            
        }
    }
}
