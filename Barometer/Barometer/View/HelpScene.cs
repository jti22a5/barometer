﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Barometer.View
{
    /// <summary>
    /// Verwerkt de weergave voor de uitleg over de applicatie
    /// </summary>
    public class HelpScene
    {
        /// <summary>
        /// De beschrijving over de applicatie
        /// </summary>
        public String Description { get; set; }

    }
}
