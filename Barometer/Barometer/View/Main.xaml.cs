﻿using Barometer.Model;
using Bing.Maps;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Barometer.Controller;
using Windows.ApplicationModel.Resources.Core;
using System.Globalization;
using Windows.Devices.Geolocation;
using Windows.UI.Core;
using Windows.UI.Xaml.Media.Imaging;

namespace Barometer.View
{
    /// <summary>   
    /// Een visuele weergave van de functionaliteiten 
    /// </summary>
    public sealed partial class Main : Page
    {

        public delegate void GPSLocationHandler(object sender);
        private DispatcherTimer mapInitTimer = new DispatcherTimer();
        
        public Button SettingsButton
        {
            get { return btnSettings; }
        }

        public Frame SightFrame
        {
            get { return frmSight; }
        }
        public Frame TutorialFrame
        {
            get { return frmTutorial; }
        }

        private Pushpin locationIcon;
        private MapPolyline m_pWalkedRouteLine;

        /// <summary>
        /// Constructor
        /// </summary>
        public Main()
        {
            this.InitializeComponent();
            
            Controller.Barometer.getBingMapsHandler().SetMap(BingMap);
            Controller.Barometer.getBingMapsHandler().Initialize();
            Controller.Barometer.getGUIController().setMainPage(this);
            Controller.Barometer.getInputHandler().LocationTriggeredEvent += Main_LocationTriggeredEvent;

            Controller.Barometer.getGUIController().setDatabaseData();
            locationIcon = new Pushpin()
            {
                Text = "X",                
                Background = new SolidColorBrush(Windows.UI.Colors.Red)
            };
            

            DrawCurrentLocation(new Location(0.0, 0.0));    


            //this.btnCurrRoute.Click += Model.UserInput.btnCurrRoute_Click;
            this.btnLanguage.Click += Model.UserInput.btnLanguage_Click;
            this.btnStopRoute.Click += Model.UserInput.btnStopRoute_Click;
            this.btnSettings.Click += Model.UserInput.btnSettings_Click;
            this.btnHelp.Click += Model.UserInput.btnHelp_Click;

            this.btnLogin.Click += Model.UserInput.btnLogin_Click;
            this.btnSave_Session.Click += Model.UserInput.btnSaveSession_Click;
            this.btnLoad_Session.Click += Model.UserInput.btnLoadSession_Click;

            updateStatistics();            

            DispatcherTimer updateTimer = new DispatcherTimer();
            updateTimer.Tick += updateTimer_Tick;
            updateTimer.Interval = new TimeSpan(0, 1, 0);
            updateTimer.Start();            
            mapInitTimer.Tick += mapInitTimer_Tick;
            mapInitTimer.Interval = new TimeSpan(0, 0, 25);
            mapInitTimer.Start();
            // Initialize map layer for walked route
            m_pWalkedRouteLine = new MapPolyline();
            m_pWalkedRouteLine.Width = 5;
            m_pWalkedRouteLine.Color = Windows.UI.Color.FromArgb(255, 255, 0, 0);
            MapShapeLayer pMapShapeLayer = new MapShapeLayer();
            pMapShapeLayer.Shapes.Add(m_pWalkedRouteLine);
            BingMap.ShapeLayers.Add(pMapShapeLayer);
            this.Loaded += Main_Loaded;
        }

        void Main_Loaded(object sender, RoutedEventArgs e)
        {
            if (Controller.Barometer.getSession().Route == null)
            {
                Controller.Barometer.getGUIController().askForSession();
            }
        }

        void Main_LocationTriggeredEvent(Sight sight)
        {       
            changeSight(sight);
        }

        public async void changeSight(Sight sight)
        {
            if(sight != null){
                if (!sight.State.Equals(Sight.EnumState.Viewed))    //Stop the spamming
                {
                    await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, new DispatchedHandler(
                        () =>
                        {
                            Controller.Barometer.getBingMapsHandler().ChangePushPinState(sight);                                                        
                            Controller.Barometer.getGUIController().showSightInfo(sight);
                        }));
                }
            }
        }

        void mapInitTimer_Tick(object sender, object e)
        {
            if (!Controller.Barometer.MapReadyFlag)
            {
                Controller.Barometer.MapReadyFlag = true;
                updateStatistics();
                mapInitTimer.Stop();                
            }
        }

        void updateTimer_Tick(object sender, object e)
        {            
            updateStatistics();
            if(Controller.Barometer.getSession().Route != null){
                Barometer.Controller.Barometer.getDatabaseHandler().SaveSession(Barometer.Controller.Barometer.getSession());
            }            
        }

        /// <summary>
        /// Update battery level
        /// </summary>
        private void updateBattery()
        {
            if (Controller.Barometer.getSession().Route != null)
            {
                this.lblBatteryStateResult.Text = Controller.BatteryController.getBatteryLevel() + "%";
                if (Controller.BatteryController.getBatteryLevel() < 50 && !Controller.BatteryController.MessageFlag)
                {
                    Controller.Barometer.ShowError(Controller.Barometer.getSession().RMap.GetValue("BatteryTitle", Controller.Barometer.getSession().Context).ValueAsString, Controller.Barometer.getSession().RMap.GetValue("BatteryDescription", Controller.Barometer.getSession().Context).ValueAsString);
                    Controller.BatteryController.MessageFlag = true;
                }
                else if (Controller.BatteryController.getBatteryLevel() > 50)
                {
                    Controller.BatteryController.MessageFlag = false;
                }
            }
        }        

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
        }

        private void updateStatistics()
        {
            this.lblTotalDistanceResult.Text = Math.Round(Controller.Barometer.getBingMapsHandler().GetTotalDistance(),3).ToString();
            this.lblTimeToGoResult.Text = String.Format("{0:00}:{1:00}", (int)(Controller.Barometer.getBingMapsHandler().GetRemainingTime() / 60), Controller.Barometer.getBingMapsHandler().GetRemainingTime() % 60);
            if(Controller.Barometer.getSession().Route != null)
            {
                if (Controller.Barometer.getSession().Route.Name != null)
                {
                    setRouteName(Controller.Barometer.getSession().Route.Name);
                }                
            }            
            updateBattery();
            if (Controller.Barometer.getGUIController().OutOfBoundsFlag)
            {
                this.lblOutOfBoundsState.Text = Controller.Barometer.getSession().RMap.GetValue("outOfBounds", Controller.Barometer.getSession().Context).ValueAsString;
            }
            else
            {
                this.lblOutOfBoundsState.Text = "";
            }
        }

        /// <summary>
        /// Tekent de gegeven route op de Bing map, dit wordt gedaan door pushpins met elkaar te verbinden met een lijn die door de straten gaat. Deze lijn wordt berekent in de Bing Maps API
        /// </summary>
        /// <param name="route">De route die getekend moet worden</param>
        public void DrawRoute(Route route)
        {

        }

        public Map GetMapObject()
        {
            return BingMap;
        }

        /// <summary>
        /// Tekent de huidige GPS locatie van de gebruiker op de kaart
        /// </summary>
        public void DrawCurrentLocation(Location position)
        {
            // TO DO: GPSIcon is a page, you don't want to add that. You want a pushpin with a different image
            // e.g. http://nocture.dk/2012/11/04/custom-pushpin-icon-for-windows-store-apps-with-bing-maps-and-c/
            BingMap.SetView(position, 13);
            //GPSIcon icon = new GPSIcon();            

            BingMap.Children.Add(locationIcon);
            MapLayer.SetPosition(locationIcon, position);
            BingMap.SetView(position);
        }

        public void setRouteName(string strName)
        {            
            this.lblCurrRouteResult.Text = strName;
        }

        /// <summary>
        /// Updates walked route
        /// </summary>
        /// <param name="pGeoposition"></param>
        private void updateWalkedRoute(Geoposition pGeoposition)
        {
            m_pWalkedRouteLine.Locations.Add(new Location(pGeoposition.Coordinate.Latitude, pGeoposition.Coordinate.Longitude));
        }

        public async void geolocatorPositionChanged(Geolocator sender, PositionChangedEventArgs args)
        {
            await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, new DispatchedHandler(
                () =>
                {
                    displayPosition(this, args);
                    updateWalkedRoute(args.Position);
                }));
        }

        /// <summary>
        /// Geeft de nieuwe coordinaten door en past de locatie van de pushpin (locationIcon) aan op de BingMap kaart.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void displayPosition(object sender, PositionChangedEventArgs args)
        {
            Location loc = new Location(args.Position.Coordinate.Latitude, args.Position.Coordinate.Longitude);
            MapLayer.SetPosition(locationIcon, loc);
            if((bool)cbxFollowGps.IsChecked)
            {
                BingMap.SetView(loc);
            }            
        }

        public void ToggleLanguage(int index)
        {
            Windows.UI.Xaml.Media.Imaging.BitmapImage img = new Windows.UI.Xaml.Media.Imaging.BitmapImage();
            img.UriSource = new Uri("ms-appx:///Assets/" + Controller.Barometer.getGUIController().languageFlagImages[index]);
            imgLanguage.Source = img;

            ResourceMap rmap = Controller.Barometer.getSession().RMap;
            ResourceContext context = Controller.Barometer.getSession().Context;
            this.lblCurrRoute.Text = rmap.GetValue("currentRoute", context).ValueAsString;
            this.lblLanguage.Text = rmap.GetValue("language", context).ValueAsString;
            this.lblTotalDistance.Text = rmap.GetValue("totalDistance", context).ValueAsString;            
            this.lblTimeToGo.Text = rmap.GetValue("timeToGo", context).ValueAsString;
            this.lblBatteryState.Text = rmap.GetValue("batteryState", context).ValueAsString;
            this.lblStop.Text = rmap.GetValue("stopRoute", context).ValueAsString;
            this.lblHelp.Text = rmap.GetValue("help", context).ValueAsString;
            this.cbxFollowGps.Content = rmap.GetValue("toggleGPS", context).ValueAsString;
            if(this.lblOutOfBoundsState.Text != "")
            {
                this.lblOutOfBoundsState.Text = Controller.Barometer.getSession().RMap.GetValue("outOfBounds", Controller.Barometer.getSession().Context).ValueAsString;
            }
        }

        public void ToggleStopButton(bool setting)
        {
            string text = "";
            if(Controller.Barometer.getSession().LanguageId == 1)
            {
                text = (setting) ? "Cancel current route" : "Route selection";
            }
            else if (Controller.Barometer.getSession().LanguageId == 0)
            {
                text = (setting) ? "Route stopzetten" : "Route selectie";
            }            
            this.lblStop.Text = text;
        }

        private void imgVVV_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Controller.Barometer.ShowError(Controller.Barometer.getSession().RMap.GetValue("VVVTitle", Controller.Barometer.getSession().Context).ValueAsString, Controller.Barometer.getSession().RMap.GetValue("VVVDescription", Controller.Barometer.getSession().Context).ValueAsString);
        }

        private void imgAGS_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Controller.Barometer.ShowError(Controller.Barometer.getSession().RMap.GetValue("AGSTitle", Controller.Barometer.getSession().Context).ValueAsString, Controller.Barometer.getSession().RMap.GetValue("AGSDescription", Controller.Barometer.getSession().Context).ValueAsString);
        }

        private void btnHelp_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
