﻿using Barometer.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Barometer.View
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Tutorial : Page
    {
#pragma warning disable 1591

        private List<String> InstructionList;
        private List<String> InstructionTitle;
        private int Page;
        private int TotalPages;
        private Windows.ApplicationModel.Resources.Core.ResourceContext context;
        private Windows.ApplicationModel.Resources.Core.ResourceMap rmap;

        /// <summary>
        /// Help function for the application
        /// </summary>
        public Tutorial()
        {
            this.InitializeComponent();
            this.context = Controller.Barometer.getSession().Context;
            this.rmap = Controller.Barometer.getSession().RMap;
            btnNext.Content = rmap.GetValue("next", context).ValueAsString;
            btnPrevious.Content = rmap.GetValue("previous", context).ValueAsString;

            InstructionList = new List<String>();
            InstructionTitle = new List<String>();
            FillIntroductionList();
            FillIntroductionTitles();
            
            //Initialize page data
            TotalPages = 6;
            Page = 1;
            
            lblInstruction.Text = InstructionList[0];
            lblTitle.Text = InstructionTitle[0];
            lblPage.Text = Page + "/" + TotalPages;
        }

        private void setMarginRectangles(int left, int top, int right, int down, Windows.UI.Xaml.Shapes.Rectangle rectangle)
        {
            rectangle.Margin = new Thickness(left,top,right,down);
        }

        /// <summary>
        /// Vult de titel lijst met strings uit de gekozen taal.
        /// </summary>
        private void FillIntroductionTitles()
        {
            InstructionTitle.Add(rmap.GetValue("currentRoute", context).ValueAsString);
            InstructionTitle.Add(rmap.GetValue("language2", context).ValueAsString);
            InstructionTitle.Add(rmap.GetValue("stopRoute", context).ValueAsString);
            InstructionTitle.Add(rmap.GetValue("help", context).ValueAsString);
            InstructionTitle.Add(rmap.GetValue("data", context).ValueAsString);
            InstructionTitle.Add(rmap.GetValue("map", context).ValueAsString);
        }


        /// <summary>
        /// Vult de instructie lijst met strings uit de gekozen taal.
        /// </summary>
        private void FillIntroductionList()
        {
            InstructionList.Add(rmap.GetValue("instructionCurrentRoute", context).ValueAsString);
            InstructionList.Add(rmap.GetValue("instructionLanguage", context).ValueAsString);
            InstructionList.Add(rmap.GetValue("instructionStopRoute", context).ValueAsString);
            InstructionList.Add(rmap.GetValue("instructionTutorial", context).ValueAsString);
            InstructionList.Add(rmap.GetValue("instructionData", context).ValueAsString);
            InstructionList.Add(rmap.GetValue("instructionMap", context).ValueAsString);
        }

        /// <summary>
        /// Change the current tutorial page
        /// </summary>
        /// <param name="recPage"></param>
        /// <param name="next"></param>
        public void getNext(int recPage, bool next)
        {
            if(recPage < TotalPages && next && !(Page > InstructionList.Count))
            {
                Page += 1;
                                
                lblInstruction.Text = InstructionList[Page - 1];
                lblTitle.Text = InstructionTitle[Page - 1];
                lblPage.Text = Page + "/" + TotalPages.ToString();

                btnNext.Content = (Page == InstructionList.Count) ? rmap.GetValue("close", context).ValueAsString : rmap.GetValue("next", context).ValueAsString;                

            }
            else if(!next)
            {
                if(Page == 1)
                {
                    return;
                }
                Page -= 1;

                btnNext.Content = rmap.GetValue("next", context).ValueAsString;
               
                lblPage.Text = Page + "/" + TotalPages;
                lblInstruction.Text = InstructionList[Page - 1];
                lblTitle.Text = InstructionTitle[Page - 1];
                
            }
            else
            {
                //End of the tutorial
                Barometer.Controller.Barometer.getGUIController().hideTutorial();
            }
            switch(Page)
            {
                case 1:
                    setMarginRectangles(1050, 105, 11, 0, rctDown);
                    setMarginRectangles(0, 0, 316, 0, rctLeft);
                    setMarginRectangles(1355, 0, 0, 0, rctRight);
                    setMarginRectangles(1050, 0, 11, 748, rctUp);
                    break;

                case 2:
                    setMarginRectangles(1050, 230, 11, 0, rctDown);
                    setMarginRectangles(0, 0, 316, 0, rctLeft);
                    setMarginRectangles(1355, 0, 0, 0, rctRight);
                    setMarginRectangles(1050, 0, 0, 648, rctUp);
                    break;
                case 3:
                    setMarginRectangles(1050, 345, 11, 0, rctDown);
                    setMarginRectangles(0, 0, 316, 0, rctLeft);
                    setMarginRectangles(1355, 0, 0, 0, rctRight);
                    setMarginRectangles(1050, 0, 0, 548, rctUp);
                    break;
                case 4:
                    setMarginRectangles(1050, 450, 11, 0, rctDown);
                    setMarginRectangles(0, 0, 316, 0, rctLeft);
                    setMarginRectangles(1355, 0, 0, 0, rctRight);
                    setMarginRectangles(1050, 0, 0, 448, rctUp);
                    break;
                case 5:
                    setMarginRectangles(1050, 760, 11, 0, rctDown);
                    setMarginRectangles(0, 0, 316, 0, rctLeft);
                    setMarginRectangles(1355, 0, 0, 0, rctRight);
                    setMarginRectangles(1050, 0, 0, 177, rctUp);
                    break;
                case 6:
                    setMarginRectangles(1050, 20, 11, 0, rctDown);
                    setMarginRectangles(0, 0, 1356, 0, rctLeft);
                    setMarginRectangles(1022, 0, 0, 0, rctRight);
                    setMarginRectangles(1050, 0, 11, 748, rctUp);
                    break;

            }

        }
                
        /// <summary>
        /// Go to the next tutorial page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            getNext(Page, true);
        }

        /// <summary>
        /// Go to the previous tutorial page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPrevious_Click(object sender, RoutedEventArgs e)
        {
            getNext(Page, false);
        }

#pragma warning restore 1591
    }
}
