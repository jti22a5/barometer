﻿using Barometer.Common;
using Barometer.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace Barometer.View
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class SightInfo : Page
    {

        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();
        private bool pageLoaded = false;

        /// <summary>
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// NavigationHelper is used on each page to aid in navigation and 
        /// process lifetime management
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }


        public SightInfo(Sight pSight)
        {
            this.InitializeComponent();            
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += navigationHelper_LoadState;
            this.navigationHelper.SaveState += navigationHelper_SaveState;
            
            // TO DO: due do this not being serialized, both of the vars are null
            Windows.ApplicationModel.Resources.Core.ResourceMap rmap = Controller.Barometer.getSession().RMap;
            Windows.ApplicationModel.Resources.Core.ResourceContext context = Controller.Barometer.getSession().Context;
            
            btnBack.Label = rmap.GetValue("back", context).ValueAsString;            
            this.lblOpeningDesc.Text = rmap.GetValue("infoOpenTimes", context).ValueAsString;
            this.lblBuildYear.Text = rmap.GetValue("infoConYear", context).ValueAsString;

            this.lblSightTitle.Text = (pSight.Name != null) ? pSight.Name : this.lblSightTitle.Text = rmap.GetValue("infoUnknown", context).ValueAsString;

            if(pSight.Image != null)
            { 
                lblScrollPlease.Text = (pSight.Image.Length > 1) ? rmap.GetValue("infoScrollPlease", context).ValueAsString : "";
            }

            if (pSight.Description == null)
            {
                if (Controller.Barometer.getSession().RMap.ContainsKey("sight_"+pSight.Name))
                {
                    this.lblDescValue.Text = Controller.Barometer.getSession().RMap.GetValue("sight_"+pSight.Name, Controller.Barometer.getSession().Context).ValueAsString;  //pSight.Description; 
                }
                else
                {
                    this.lblDescValue.Text = Controller.Barometer.getSession().RMap.GetValue("infoUnknown", Controller.Barometer.getSession().Context).ValueAsString;  //pSight.Description; 
                }
                
            }
            else{
                this.lblDescValue.Text = pSight.Description;
            }

            if (pSight.OpeningDescription != null)
            {
                this.lblOpeningDescValue.Text = pSight.OpeningDescription;
            }
            else {
                this.lblOpeningDescValue.Text = rmap.GetValue("infoUnknown", context).ValueAsString;
            }

            if (this.lblBuildYearValue != null)
            {
                if (!pSight.BuildYear.Year.Equals(1))
                {
                    this.lblBuildYearValue.Text = pSight.BuildYear.ToString("dd-MM-yyyy");
                }
                else{
                    this.lblBuildYearValue.Text = rmap.GetValue("infoUnknown", context).ValueAsString;
                }
            }
            else
            {
                this.lblBuildYearValue.Text = rmap.GetValue("infoUnknown", context).ValueAsString;
            }

            if(pSight.Image != null)
            {
                foreach (ImageSource img in pSight.Image)
                {
                    flipViewer.Items.Add(img);
                }
            }            
                        
            //if (pSight.Media == null
            //    && pSight.Name != null)
            //{
            //    pSight.Media = new Dictionary<Sight.MediaEnum, Media>();
            //    if (pSight.ImagePath == null)
            //    {
            //        if (pSight.Sequence == 12)
            //        {
            //            pSight.addHardcodedImage("13.jpg");
            //        }
            //        else if (pSight.Sequence > 12)
            //        {
            //            pSight.addHardcodedImage(pSight.Sequence + ".jpg");
            //        }
            //    }
            //}


            // Media
            //if (pSight.Media != null) 
            //{ 
            //    foreach (KeyValuePair<Model.Sight.MediaEnum, Media> pKeyValuePair in pSight.Media) 
            //    {
            //        switch (pKeyValuePair.Key)
            //        {
            //            case Sight.MediaEnum.MEDIA_IMAGE:

            //                    pMedia.Children.Add(new Windows.UI.Xaml.Controls.Image()
            //                    {
            //                        Stretch = Windows.UI.Xaml.Media.Stretch.Uniform,
            //                        MaxWidth = 300,
            //                        MaxHeight = 300,
            //                        Source = ((Model.Image)pKeyValuePair.Value).SourceImage
            //                    });

            //                break;
            //        }
            //    }
            //}
        }

        /// <summary>
        /// Populates the page with content passed during navigation. Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session. The state will be null the first time a page is visited.</param>
        private void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {            
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void navigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region NavigationHelper registration

        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// 
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="GridCS.Common.NavigationHelper.LoadState"/>
        /// and <see cref="GridCS.Common.NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            Controller.Barometer.getGUIController().hideSightInfo();
        }
    }
}
